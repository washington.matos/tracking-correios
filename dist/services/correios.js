"use strict";

var axios = require('axios');

var _get = require('lodash/get');

var _chunk = require('lodash/chunk');

var _flatten = require('lodash/flatten');

var _require = require('xml2js'),
    parseStringPromise = _require.parseStringPromise;

var _require2 = require('../utils/helpers'),
    expand = _require2.expand,
    arrayOf = _require2.arrayOf;

var TrackingError = require('../errors/tracking');

var _require3 = require('../utils/consts'),
    CORREIOS_URL = _require3.CORREIOS_URL;

function fetchTracking(objects, configParams) {
  return new Promise(function (resolve, reject) {
    Promise.resolve(objects).then(fetchFromCorreios).then(parseFetchOutput).then(resolvePromise)["catch"](rejectWithError);

    function fetchFromCorreios(objects) {
      var callsChunked = _chunk(objects, configParams.limit);

      return Promise.all(callsChunked.map(fetchFromAPI));
    }

    function fetchFromAPI(objects) {
      var options = {
        headers: {
          'Content-Type': 'text/xml; charset=utf-8',
          'Cache-Control': 'no-cache'
        }
      };
      return axios.post(CORREIOS_URL, createSOAPEnvelope(objects), options);
    }

    function parseFetchOutput(responses) {
      return Promise.all(responses.map(parseSingleFetch)).then(mergeAllResponses);
    }

    function mergeAllResponses(responses) {
      return _flatten(responses);
    }

    function parseSingleFetch(response) {
      if (response.status === 200) {
        return Promise.resolve(response.data).then(parseXml).then(extractSuccessObject).then(fixEvent);
      }

      return Promise.resolve(response.data).then(parseXml).then(extractErrorObject).then(throwErrorObject);
    }

    function parseXml(text) {
      return parseStringPromise(text)["catch"](function () {
        return new TrackingError({
          message: 'Não foi possível interpretar o XML de resposta.',
          type: 'service_error',
          errors: [{
            message: 'Ocorreu um erro ao tratar o XML retornado pela API dos Correios.',
            service: 'parsing_error'
          }]
        });
      });
    }

    function extractSuccessObject(object) {
      return _get(object, 'soapenv:Envelope.soapenv:Body[0].ns2:buscaEventosListaResponse[0].return[0].objeto').map(expand);
    }

    function extractErrorObject(object) {
      return expand(_get(object, 'soapenv:Envelope.soapenv:Body[0].soapenv:Fault[0].faultstring[0]'));
    }

    function fixEvent(object) {
      return object.map(function (item) {
        item.evento = arrayOf(item.evento);
        return item;
      });
    }

    function throwErrorObject(faultString) {
      throw new TrackingError({
        message: 'Erro no serviço do Correios.',
        type: 'service_error',
        errors: [{
          message: "O servi\xE7o do Correios retornou o seguinte erro: ".concat(faultString),
          service: 'service_error'
        }]
      });
    }

    function createSOAPEnvelope(objects) {
      var envelope = "<?xml version=\"1.0\"?>\n<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:res=\"http://resource.webservice.correios.com.br/\">\n   <soapenv:Header/>\n   <soapenv:Body>\n      <res:buscaEventosLista>\n";

      if (configParams.username && configParams.password) {
        envelope += "         <usuario>".concat(configParams.username, "</usuario>\n         <senha>").concat(configParams.password, "</senha>\n");
      }

      envelope += "         <tipo>".concat(configParams.type, "</tipo>\n         <resultado>").concat(configParams.result, "</resultado>\n         <lingua>").concat(configParams.language, "</lingua>\n");
      objects.forEach(function (object) {
        envelope += "         <objetos>".concat(object, "</objetos>\n");
      });
      envelope += "      </res:buscaEventosLista>\n   </soapenv:Body>\n</soapenv:Envelope>";
      return envelope;
    }

    function resolvePromise(objects) {
      resolve(objects);
    }

    function rejectWithError() {
      var error = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
      var trackingError = new TrackingError({
        message: error.message,
        type: error.type,
        errors: error.errors
      });

      if (error.isAxiosError && _get(error, 'response.status') === 500) {
        trackingError.message = 'Erro ao se conectar ao o serviço dos Correios.';
        trackingError.type = 'service_error';
        trackingError.errors = [{
          message: "Ocorreu um erro ao se conectar ao servi\xE7o dos Correios: ".concat(error.message),
          service: 'service_error'
        }];
      }

      reject(trackingError);
    }
  });
}

module.exports = {
  fetchTracking: fetchTracking
};