"use strict";

var _isString = require('lodash/isString');

var _require = require('./consts'),
    VALID_INITIALS = _require.VALID_INITIALS;

function matchesPattern(object) {
  return /^([A-Z]{2}[0-9]{9}[A-Z]{2}){1}$/.test(object);
}

function category(object) {
  if (!matchesPattern(object) || !_isString(object)) {
    return;
  }

  return VALID_INITIALS[object.substr(0, 2)];
}

function isValid(object) {
  return category(object) !== undefined;
}

module.exports = {
  category: category,
  isValid: isValid
};