"use strict";

function TrackingError() {
  var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  this.name = 'TrackingError';
  this.message = options.message;
  this.type = options.type;
  this.errors = options.errors;
}

TrackingError.prototype = Object.create(Error.prototype);
TrackingError.prototype.constructor = TrackingError;
module.exports = TrackingError;