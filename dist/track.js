"use strict";

var _filter = require('lodash/filter');

var _extend = require('lodash/assignIn');

var _difference = require('lodash/difference');

var TrackingError = require('./errors/tracking');

var _require = require('./services/correios'),
    fetchTracking = _require.fetchTracking;

var _require2 = require('./utils/tracking-helpers'),
    isValid = _require2.isValid,
    category = _require2.category;

var _require3 = require('./utils/helpers'),
    arrayOf = _require3.arrayOf;

var _require4 = require('./utils/consts'),
    MAX_OBJECTS_CORREIOS = _require4.MAX_OBJECTS_CORREIOS;

function track(objects) {
  var configParams = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
  // default params
  configParams = _extend({
    username: 'ECT',
    password: 'SRO',
    type: 'L',
    result: 'T',
    language: '101',
    limit: 5000,
    filter: true
  }, configParams);
  return new Promise(function (resolve, reject) {
    Promise.resolve({
      objects: objects,
      configParams: configParams
    }).then(validateParams).then(filterObjects).then(validateObjects).then(fetchFromCorreios).then(resolvePromise)["catch"](rejectWithError);

    function resolvePromise(objects) {
      resolve(objects);
    }

    function rejectWithError(error) {
      reject(new TrackingError({
        message: error.message,
        type: error.type,
        errors: error.errors
      }));
    }

    function validateParams(params) {
      if (params.configParams.type && params.configParams.result && params.configParams.language && params.configParams.limit > 0 && params.configParams.limit <= MAX_OBJECTS_CORREIOS && typeof params.configParams.filter === 'boolean') {
        return params;
      }

      throw new TrackingError({
        message: 'Erro ao validar os parâmetros.',
        type: 'validation_error',
        errors: [{
          message: 'Type, result e language não podem ser undefined, filter deve ser boolean',
          service: 'param_validation'
        }]
      });
    }

    function filterObjects(params) {
      params.objects = arrayOf(params.objects);

      if (params.configParams.filter) {
        params.objects = filter(params.objects);
      }

      return params;
    }

    function validateObjects(params) {
      if (params.objects.length > 0) {
        return params;
      }

      throw new TrackingError({
        message: 'Erro ao validar os objetos.',
        type: 'validation_error',
        errors: [{
          message: 'Nenhum objeto válido para pesquisa.',
          service: 'objects_validation'
        }]
      });
    }

    function fetchFromCorreios(params) {
      return fetchTracking(params.objects, params.configParams);
    }
  });
}

function validate(objects) {
  objects = arrayOf(objects);
  var filtered = filter(objects);
  return {
    valid: filtered,
    invalid: _difference(objects, filtered)
  };
}

function filter(objects) {
  objects = arrayOf(objects);
  return _filter(objects, isValid);
}

module.exports = {
  track: track,
  validate: validate,
  isValid: isValid,
  category: category,
  filter: filter
};